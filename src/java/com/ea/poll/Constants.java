/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Tanmay Patil
 */
public class Constants {

    public final static Properties PROPERTIES = new Properties();

    static {
        InputStream inputStream = Constants.class.getResourceAsStream("Config.properties");
        try {
            PROPERTIES.load(inputStream);
            inputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public final static String OLD_QUESTIONS_STORAGE_FILE = PROPERTIES.getProperty("old question storage file");
    public final static String QUESTIONS_STORAGE_FILE = PROPERTIES.getProperty("question storage file");
    
    
    public final static String VERSION_NUMBER = PROPERTIES.getProperty("version");
}
