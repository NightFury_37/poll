/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.storage;

import com.ea.poll.Constants;
import com.ea.poll.model.Choice;
import com.ea.poll.model.Question;
import com.ea.poll.model.User;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author tpatil
 */
public class StorageHandler {

    public static Vector<Question> readQuestions(String fileName) {
        if (fileName.equals(Constants.OLD_QUESTIONS_STORAGE_FILE)) {
            return readQuestionsOld(fileName);
        } else if (fileName.equals(Constants.QUESTIONS_STORAGE_FILE)) {
            return readQuestionsNew(fileName);
        } else {
            return new Vector<Question>();
        }
    }

    public static Vector<Question> readQuestionsOld(String fileName) {
        //try {
            //File file = new File(URLDecoder.decode(StorageHandler.class.getResource(fileName).getFile(), "UTF-8"));
            File file = new File(fileName);
            if (file.exists()) {
                ObjectInputStream objectInputStream = null;
                try {
                    Vector<Question> questions;
                    objectInputStream = new ObjectInputStream(new FileInputStream(file));
                    questions = (Vector<Question>) objectInputStream.readObject();
                    return questions;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    return new Vector<Question>();
                } catch (IOException ex) {
                    Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    return new Vector<Question>();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    return new Vector<Question>();
                } finally {
                    try {
                        objectInputStream.close();
                    } catch (IOException ex) {
                        Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                return new Vector<Question>();
            }
        //}
            /* catch (UnsupportedEncodingException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
            return new Vector<Question>();
        }*/
    }

    public static Vector<Question> readQuestionsNew(String fileName) {
        try {
            //File file = new File(URLDecoder.decode(StorageHandler.class.getResource(fileName).getFile(), "UTF-8"));
            File file = new File(fileName);
            if (file.exists()) {
                // Some XML initialisations
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(file);
                document.getDocumentElement().normalize();
                String version = (document.getElementsByTagName("header").getLength() != 0) ? (((Element) document.getElementsByTagName("header").item(0)).getAttribute("version")) : "-1";
                if (version.equals(Constants.VERSION_NUMBER)) {
                    NodeList questionNodeList = document.getElementsByTagName("question");
                    Vector<Question> questions = new Vector<Question>();
                    for (int i = 0; i < questionNodeList.getLength(); i++) {
                        Node questionNode = questionNodeList.item(i);
                        if (questionNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element questionElement = (Element) questionNode;
                            Question question = new Question(questionElement.getAttribute("text"), Integer.parseInt(questionElement.getAttribute("id")));
                            NodeList choiceNodeList = questionElement.getElementsByTagName("choice");
                            for (int j = 0; j < choiceNodeList.getLength(); j++) {
                                Node choiceNode = choiceNodeList.item(j);
                                if (choiceNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element choiceElement = (Element) choiceNode;
                                    Choice choice = new Choice(question, choiceElement.getAttribute("name"));
                                    NodeList voteNodeList = choiceElement.getElementsByTagName("vote");
                                    for (int k = 0; k < voteNodeList.getLength(); k++) {
                                        Node voteNode = voteNodeList.item(k);
                                        if (voteNode.getNodeType() == Node.ELEMENT_NODE) {
                                            Element voteElement = (Element) voteNode;
                                            choice.addUserVote(new User(((Element) voteElement.getElementsByTagName("user").item(0)).getAttribute("name")));
                                        }
                                    }
                                    question.addChoice(choice);
                                }
                            }
                            questions.add(question);
                        }
                    }
                    return questions;
                } else {
                    return new Vector<Question>();
                }
            } else {
                return new Vector<Question>();
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
            return new Vector<Question>();
        } catch (SAXException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
            return new Vector<Question>();
        } catch (IOException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
            return new Vector<Question>();
        }
    }

    public static void writeQuestions(Vector<Question> questions, String fileName) {
        if (fileName.equals(Constants.OLD_QUESTIONS_STORAGE_FILE)) {
            writeQuestionsOld(questions, fileName);
        } else if (fileName.equals(Constants.QUESTIONS_STORAGE_FILE)) {
            writeQuestionsNew(questions, fileName);
        }
    }

    public static void writeQuestionsOld(Vector<Question> questions, String fileName) {
        ObjectOutputStream objectOutputStream = null;
        try {
            //File file = new File(URLDecoder.decode(StorageHandler.class.getResource(fileName).getFile(), "UTF-8"));
            File file = new File(fileName);
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(questions);
            objectOutputStream.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                objectOutputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void writeQuestionsNew(Vector<Question> questions, String fileName) {
        try {
            //File file = new File(URLDecoder.decode(StorageHandler.class.getResource(fileName).getFile(), "UTF-8"));
            File file = new File(fileName);
            // Some XML initialisations
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            // Root element
            Element rootElement = document.createElement("root");
            document.appendChild(rootElement);
            // Appending version
            Element headerElement = document.createElement("header");
            headerElement.setAttribute("version", Constants.VERSION_NUMBER);
            rootElement.appendChild(headerElement);
            // Body element - the meaty part of the poll!
            Element bodyElement = document.createElement("body");
            rootElement.appendChild(bodyElement);
            // Loop to write each question
            for (Question question : questions) {
                Element questionElement = document.createElement("question");
                questionElement.setAttribute("id", Integer.toString(question.getId()));
                questionElement.setAttribute("text", question.getQuestionText());
                // Loop for each choice
                for (Choice choice : question.getChoices()) {
                    Element choiceElement = document.createElement("choice");
                    choiceElement.setAttribute("parent", Integer.toString(choice.getQuestion().getId()));
                    choiceElement.setAttribute("name", choice.getName());
                    // Loop for each user/vote combination
                    for (User user : choice.getUsers()) {
                        Element voteElement = document.createElement("vote");
                        voteElement.setAttribute("type", "up");
                        Element userElement = document.createElement("user");
                        userElement.setAttribute("name", user.getName());
                        voteElement.appendChild(userElement);
                        choiceElement.appendChild(voteElement);
                    }
                    questionElement.appendChild(choiceElement);
                }
                bodyElement.appendChild(questionElement);
            }
            // Write to a file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource dOMSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(file);
            transformer.transform(dOMSource, streamResult);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        }/* catch (UnsupportedEncodingException ex) {
            Logger.getLogger(StorageHandler.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
}
