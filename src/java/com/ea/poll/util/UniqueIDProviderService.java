/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.util;

import com.ea.poll.Constants;
import com.ea.poll.model.Question;
import com.ea.poll.storage.StorageHandler;

/**
 *
 * @author Tanmay Patil
 */
public class UniqueIDProviderService {
    
    private UniqueIDProviderService() {
        
    }
    
    private static class UniqueIDProviderServiceHolder {
        private final static UniqueIDProviderService UNIQUE_ID_PROVIDER_SERVICE = new UniqueIDProviderService();
    }

    public static UniqueIDProviderService getInstance() {
        return UniqueIDProviderServiceHolder.UNIQUE_ID_PROVIDER_SERVICE;
    }

    public int generateNewUniqueID() {
        int id = 0;
        boolean unique = false;
        while (!unique) {
            id = (int) (Integer.MAX_VALUE * Math.random());
            unique = true;
            for (Question question : StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE)) {
                if (question.getId() == id) {
                    unique = false;
                }
            }
        }
        return id;
    }
    
}
