/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.control;

import com.ea.poll.Constants;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Tanmay Patil
 */
public class Authorizer {
    
    private Authorizer() {
        InputStream inputStream = Constants.class.getResourceAsStream("Config.properties");
        try {
            Constants.PROPERTIES.load(inputStream);
            inputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private static class AuthorizerHolder {
        private final static Authorizer AUTHORIZER = new Authorizer();
    }

    public static Authorizer getInstance() {
        return AuthorizerHolder.AUTHORIZER;
    }

    public boolean isAuthorized(String userName, String password) {
        if (userName != null && password != null) {
            if (Constants.PROPERTIES.containsKey(userName)) {
                if (Constants.PROPERTIES.getProperty(userName).equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }

}
