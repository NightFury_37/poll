/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.model;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author tpatil
 */
public class Choice implements Serializable {

    private Question question;
    private String name;
    private Vector<User> users;

    public Choice(Question question, String name) {
        this.question = question;
        this.name = name;
        users = new Vector<User>();
    }

    public Choice addUserVote(User user) {
        boolean flag = true;
        Vector<User> users;
        users = this.getUsers();
        for (User u : users) {
            if (u.getName().trim().toLowerCase().equals(user.getName().trim().toLowerCase())) {
                flag = false;
            }
        }
        if (flag == true) {
            users.add(user);
        }

        return this;
    }

    public int getNumberOfUsers() {
        return users.size();
    }

    public String getName() {
        return name;
    }

    public Question getQuestion() {
        return question;
    }

    public Vector<User> getUsers() {
        return users;
    }

    @Override
    public String toString() {
        return name;
    }
}
