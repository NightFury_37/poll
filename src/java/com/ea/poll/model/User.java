/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.model;

import java.io.Serializable;

/**
 *
 * @author tpatil
 */
public class User implements Serializable {
    
    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
