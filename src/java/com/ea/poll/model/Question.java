/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ea.poll.model;

import com.ea.poll.util.UniqueIDProviderService;
import java.io.Serializable;
import java.util.Vector;
import javax.sound.midi.Soundbank;

/**
 *
 * @author tpatil
 */
public class Question implements Serializable {

    private String questionText;
    private Vector<Choice> choices;
    private int id;

    public Question(String questionText) {
        this.questionText = questionText;
        this.choices = new Vector<Choice>();
        this.id = UniqueIDProviderService.getInstance().generateNewUniqueID();
    }

    public Question(String questionText, int id) {
        this.questionText = questionText;
        this.choices = new Vector<Choice>();
        this.id = id;
    }

    public Question addChoice(Choice choice) {
        boolean flag = true;
        for (Choice ch : choices) {
            if (ch.getName().trim().toLowerCase().equals(choice.getName().trim().toLowerCase())) {
                flag = false;
                break;
            }
        }
        if (flag == true) {
            choices.add(choice);
        }
        return this;
    }

    public void removeChoice(Choice choice) {
        choices.remove(choice);
    }

    public Vector<Choice> getChoices() {
        return choices;
    }

    public int getId() {
        return id;
    }

    public String getQuestionText() {
        return questionText;
    }

    @Override
    public String toString() {
        return questionText;
    }
}
