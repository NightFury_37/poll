<%-- 
    Document   : administratorLogout
    Created on : May 16, 2013, 2:26:06 AM
    Author     : Tanmay Patil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    session.removeAttribute("authorized");
    try {
        session.invalidate();
    } catch (IllegalStateException ex) {
        
    }
    response.sendRedirect("index.jsp");
%>

<!DOCTYPE html>
<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
    </head>
    <body>
        <div class="controlbar">
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Welcome to EA HY Web Poll</h1>
            </div>
        </div>
    </body>
</html>
