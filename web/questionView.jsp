<%-- 
    Document   : questionView
    Created on : 15 May, 2013, 1:00:45 PM
    Author     : tpatil
--%>

<%@page import="com.ea.poll.model.User"%>
<%@page import="com.ea.poll.model.Choice"%>
<%@page import="com.ea.poll.Constants"%>
<%@page import="com.ea.poll.storage.StorageHandler"%>
<%@page import="com.ea.poll.model.Question"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    boolean flag = false;
    if (session.getAttribute("authorized") != null) {
        if (session.getAttribute("authorized").toString().equals("yes")) {
            flag = true;
        }
    }
    if (!flag) {
        session.removeAttribute("authorized");
        response.sendRedirect("administratorLogin.jsp");
    }
%>

<!DOCTYPE html>


<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>

        <title>Studio Poll</title>
        <script type = "text/javascript">

            function process(myChoice, myUser)
            {
                var str = myChoice + myUser;
                document.getElementById('output').innerHTML = str;
                document.getElementById("myHiddenChoice").value = myChoice;
                document.getElementById("myHiddenUser").value = myUser;
                saveScroll();
                document.getElementById("mainform").submit();
            }

            var get_scroll = function() {
                var x = 0, y = 0;
                if (typeof(window.pageYOffset) === 'number') {
                    //Netscape compliant
                    y = window.pageYOffset;
                    x = window.pageXOffset;
                } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                    //DOM compliant
                    y = document.body.scrollTop;
                    x = document.body.scrollLeft;
                } else if (document.documentElement &&
                        (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                    //IE6 standards compliant mode
                    y = document.documentElement.scrollTop;
                    x = document.documentElement.scrollLeft;
                }
                var obj = new Object();
                obj.x = x;
                obj.y = y;
                return obj;
            };

            function saveScroll() {
                var scroll = get_scroll();
                document.getElementById("x").value = scroll.x;
                document.getElementById("y").value = scroll.y;
            }

            function setScroll() {
                var x = "0";
            <% if (request.getParameter("x") != null) {%>
                x = "<%= request.getParameter("x")%>";
            <% }%>
                var y = "0";
            <% if (request.getParameter("y") != null) {%>
                y = "<%= request.getParameter("y")%>";
            <% }%>

                if (typeof x !== 'undefined')
                    window.scrollTo(x, y);
            }
        </script>
    </head>
    <body onload = "setScroll();">

        <div class="controlbar">
            <div class="loginbutton"><a href="administratorLogout.jsp">Administrator Logout</a></div>
            <div class="loginbutton"><a href="adminView.jsp">Back to administration...</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <div class="loginbutton"><b>Hello <%= session.getAttribute("userName")%></b></div>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Welcome to EA HY Web Poll</h1>
            </div>
        </div>

        <div class="myform">
            <form name="mainform" action="questionView.jsp?questionId=<%= request.getParameter("questionId")%>" method="POST" id="mainform">
                <input type="hidden" name="myHiddenChoice" id="myHiddenChoice" value="">
                <input type="hidden" name="myHiddenUser" id="myHiddenUser" value="">
                <input name="x" id="x" type="hidden" value="" />
                <input name="y" id="y" type="hidden" value="" />
                <div style="padding-left: 20px">
                    <%
                        String message = "Question not found.";
                        Vector<Choice> choices = new Vector<Choice>();
                        Vector<Question> questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);
                        try {
                            if (request.getParameter("questionId") != null) {
                                int id = Integer.parseInt(request.getParameter("questionId"));
                                for (Question question : questions) {
                                    if (question.getId() == id) {
                                        if (request.getParameter("myHiddenChoice") != null && request.getParameter("myHiddenUser") != null) {
                                            for (Choice choice : question.getChoices()) {
                                                if (choice.toString().equals(request.getParameter("myHiddenChoice").replace("&#39;", "'"))) {
                                                    for (int i = 0; i < choice.getNumberOfUsers(); i++) {
                                                        if (choice.getUsers().elementAt(i).getName().equals(request.getParameter("myHiddenUser").replace("&#39;", "'"))) {
                                                            choice.getUsers().remove(i);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        message = "Question : " + question.getQuestionText();
                                        choices = question.getChoices();
                                    }
                                }
                            }
                        } catch (NumberFormatException ex) {
                        }
                        StorageHandler.writeQuestions(questions, Constants.QUESTIONS_STORAGE_FILE);
                    %>
                    <h1><%= message%></h1>
                </div>

                <div style="padding-left: 40px">
                    <% for (Choice choice : choices) {%>
                    <div style="border: 1px #AAAAAA outset; margin-bottom: 10px; margin-right: 40px; padding: 20px; padding-top: 10px">
                        <h3><%= choice.getName() + " (" + choice.getNumberOfUsers() + (choice.getNumberOfUsers() == 1 ? " user)" : " users)")%></h3>
                        <% for (User user : choice.getUsers()) {%>
                        <div style="float: left; margin-bottom: 10px; margin-right: 10px; padding: 5px; width: auto; height: auto; background: #AAAAAA; color: #000000; line-height: 1em; border: 1px #EEEEEE inset">
                            <div style="float: left; padding: 2px; padding-right: 5px">
                                <%= user.getName()%>
                            </div>
                            <div style="float: right" onclick='process("<%= choice.toString().replace("'", "&#39;")%>", "<%= user.getName().replace("'", "&#39;")%>")'>
                                <img src='resources/Close_Box_Red.png'/>
                            </div>
                        </div>
                        <% }%>
                        <div style="clear: both"></div>
                    </div>
                    <% }%>
                </div>
            </form>
        </div>
    </body>
</html>
