<%-- 
    Document   : pollResult
    Created on : 14 May, 2013, 11:07:41 AM
    Author     : tpatil
--%>

<%@page import="com.ea.poll.Constants"%>
<%@page import="com.ea.poll.model.User"%>
<%@page import="com.ea.poll.model.Choice"%>
<%@page import="com.ea.poll.storage.StorageHandler"%>
<%@page import="com.ea.poll.model.Question"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<Question> questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);
    Vector<Integer> questionIds = new Vector<Integer>();
    boolean flag = false;
    for (Question question : questions) {
        String action = request.getParameter(question.toString() + "Text");
        if (action != null) {
            if (!action.equals("")) {
                int choicesSize = question.getChoices().size();
                question.addChoice(new Choice(question, action).addUserVote(new User(request.getParameter("voterName"))));
                if (choicesSize == question.getChoices().size()) {
                    for (Choice choice : question.getChoices()) {
                        int usersSize = choice.getNumberOfUsers();
                        if (action.trim().toLowerCase().equals(choice.toString().trim().toLowerCase())) {
                            choice.addUserVote(new User(request.getParameter("voterName")));
                            if (usersSize != choice.getNumberOfUsers()) {
                                flag = true;
                                questionIds.add(question.getId());
                            }
                        }
                    }
                } else {
                    flag = true;
                }
            } else {
                action = request.getParameter(question.toString());
                if (action != null) {
                    for (Choice choice : question.getChoices()) {
                        int usersSize = choice.getNumberOfUsers();
                        if (action.equals(choice.toString())) {
                            choice.addUserVote(new User(request.getParameter("voterName")));
                            if (usersSize != choice.getNumberOfUsers()) {
                                flag = true;
                                questionIds.add(question.getId());
                            }
                        }
                    }
                }
            }
        }
    }
    StorageHandler.writeQuestions(questions, Constants.QUESTIONS_STORAGE_FILE);
%>
<!DOCTYPE html>
<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
    </head>
    <body>
        <div class="controlbar">
            <%
                boolean localFlag = false;
                if (session.getAttribute("authorized") != null) {
                    if (session.getAttribute("authorized").toString().equals("yes")) {
                        localFlag = true;
                    }
                }
                if (localFlag) {%>
            <div class="loginbutton"><a href="administratorLogout.jsp">Administrator Logout</a></div>
            <div class="loginbutton"><a href="adminView.jsp">Back to administration...</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <div class="loginbutton"><b>Hello <%= session.getAttribute("userName")%></b></div>
            <% } else {%>
            <div class="loginbutton"><a href="administratorLogin.jsp">Administrator Login</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <% }
            %>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <% if (flag) {%>
                <h1>Thank you for your vote</h1>
            </div>
        </div>
        <div class="myform">
            <ol>
                <% overall:
                    for (Question question : questions) {
                        for (Integer id : questionIds) {
                            if (id.intValue() == question.getId()) {
                                break;
                            } else {
                                continue overall;
                            }
                        }%>
                <li>
                    <div style="padding-left: 20px">
                        <h3>
                            <%= question%>
                        </h3>
                    </div>
                    <div style="padding-left: 40px">
                        <% for (Choice choice : question.getChoices()) {%>
                        <div style="border: 1px #AAAAAA outset; margin-bottom: 10px; margin-right: 40px; padding: 5px; padding-left: 15px">
                            <%= choice + " \t(" + choice.getNumberOfUsers() + ((choice.getNumberOfUsers() != 1) ? " votes)" : " vote)")%> <br><br>
                            <% for (User user : choice.getUsers()) {%>
                            <div style="float: left; margin: 10px; margin-top: 0px; padding: 10px; width: auto; height: 10px; background: #AAAAAA; color: #000000; line-height: 1em; border: 1px #EEEEEE inset">
                                <%= user.getName()%>
                            </div>
                            <% }%>
                            <div style="clear: both"></div>
                        </div>
                        <% }%>
                </li>
                <%
                }%>
            </ol>
            <% } else {%>
            <h1>Your vote was not accepted</h1>
            <% }%>
        </div>
    </div>

</body>
</html>
