<%-- 
    Document   : index
    Created on : 13 May, 2013, 5:09:52 PM
    Author     : tpatil
--%>

<%@page import="java.io.IOException"%>
<%@page import="java.io.File"%>
<%@page import="java.io.OutputStream"%>
<%@page import="com.ea.poll.Constants"%>
<%@page import="com.ea.poll.model.Choice"%>
<%@page import="com.ea.poll.storage.StorageHandler"%>
<%@page import="java.util.Vector"%>
<%@page import="com.ea.poll.model.Question"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--%
    Process process = Runtime.getRuntime().exec;
    process.waitFor();
%-->
<!DOCTYPE html>
<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
        <script type = "text/javascript">
            window.onload = function() {
                var sets = document.getElementsByTagName('li');
                var setsCount = sets.length;
                var clearBox = function(box) {
                    box.value = '';
                };
                var clearFields = function(fieldset) {
                    var buttons = fieldset.querySelectorAll('input[type="radio"]');
                    var buttonCount = buttons.length;
                    var textBox = fieldset.querySelector('input[type="text"]');
                    var j;

                    for (j = 0; j < buttonCount; j++) {
                        buttons[j].onclick = function() {
                            clearBox(textBox);
                        };
                    }

                    textBox.onkeypress = function() {
                        for (j = 0; j < buttonCount; j++) {
                            buttons[j].checked = false;
                        }
                    };
                };
                var i;
                for (i = 0; i < setsCount; i++) {
                    clearFields(sets[i]);
                }
            };
        </script>
    </head>
    <body>
        <div class="controlbar">
            <%
                boolean flag = false;
                if (session.getAttribute("authorized") != null) {
                    if (session.getAttribute("authorized").toString().equals("yes")) {
                        flag = true;
                    }
                }
                if (flag) {%>
            <div class="loginbutton"><a href="administratorLogout.jsp">Administrator Logout</a></div>
            <div class="loginbutton"><a href="adminView.jsp">Back to administration...</a></div>
            <div class="loginbutton"><b>Hello <%= session.getAttribute("userName")%></b></div>
            <% } else {%>
            <div class="loginbutton"><a href="administratorLogin.jsp">Administrator Login</a></div>
            <% }
            %>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Welcome to EA HY Studio Poll</h1>
            </div>
        </div>

        <div class="myform">
            <form name="mainform" action="pollResult.jsp" method="POST">
                <%
                    Vector<Question> questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);
                    if (questions.size() != 0) {%>
                <div style="margin-left: 20px; padding: 10">
                    Name <sup style="color: #FFFF77">*</sup> :
                    <input type="text" name="voterName" placeholder="Enter your name" required="true"></input>
                </div>
                <% }%>
                <ol>
                    <%
                        for (Question question : questions) {
                    %>
                    <li>
                        <%= question%> <br>
                        <% for (Choice choice : question.getChoices()) {%>
                        <input type ="radio" name="<%= question%>" value="<%= choice.toString()%>"><%= choice + " \t(" + choice.getNumberOfUsers() + ((choice.getNumberOfUsers() != 1) ? " votes)" : " vote)")%> <br>
                        <% }%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="<%= question%>Text" placeholder="Enter your choice here&nbsp;">
                    </li><br>
                    <% }%>
                </ol>
                <%
                    if (questions.size() != 0) {%>
                <div style="margin-left: 20px; padding: 10">
                    <input type="submit" value="Submit your vote!" name="submitButton" />
                </div>
                <% } else {%>
                <div style="margin-left: 20px; padding: 10">
                    No questions to vote...
                </div>
                <% }%>
            </form>
        </div>
    </body>
</html>
