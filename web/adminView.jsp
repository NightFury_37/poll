<%-- 
    Document   : adminView
    Created on : 14 May, 2013, 12:05:51 PM
    Author     : tpatil
--%>

<%@page import="com.ea.poll.Constants"%>
<%@page import="com.ea.poll.model.User"%>
<%@page import="com.ea.poll.model.Choice"%>
<%@page import="com.ea.poll.storage.StorageHandler"%>
<%@page import="java.util.Vector"%>
<%@page import="com.ea.poll.model.Question"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    boolean flag = false;
    if (session.getAttribute("authorized") != null) {
        if (session.getAttribute("authorized").toString().equals("yes")) {
            flag = true;
        }
    }
    if (!flag) {
        session.removeAttribute("authorized");
        response.sendRedirect("administratorLogin.jsp");
    }
%>
<!DOCTYPE html>

<script>
    function copyToClipboard(text) {
        window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    }
</script>

<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
    </head>

    <body>
        <div class="controlbar">
            <div class="loginbutton"><a href="administratorLogout.jsp">Administrator Logout</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <div class="loginbutton"><b>Hello <%= session.getAttribute("userName")%></b></div>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Hello Admin</h1>
            </div>
        </div>

        <%
            Vector<Question> questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);

            String newQuestion = request.getParameter("newQuestionText");
            if (newQuestion != null) {
                if (!newQuestion.equals("")) {
                    questions.add(new Question(newQuestion));
                }
            }

            for (Question question : questions) {

                String[] action = request.getParameterValues(question.toString());
                for (int j = 0; j < question.getChoices().size(); j++) {
                    if (action != null) {
                        for (int i = 0; i < action.length; i++) {
                            if (action[i].equals(question.getChoices().get(question.getChoices().size() - j - 1).toString())) {
                                question.removeChoice(question.getChoices().get(question.getChoices().size() - j-- - 1));
                            }
                        }
                    }
                }
            }

            String[] action = request.getParameterValues("question");
            if (action != null) {
                for (int i = 0; i < action.length; i++) {
                    for (int j = 0; j < questions.size(); j++) {
                        if (questions.get(questions.size() - j - 1).toString().equals(action[action.length - i - 1])) {
                            questions.remove(questions.size() - j - 1);
                        }
                    }
                }
            }

            StorageHandler.writeQuestions(questions, Constants.QUESTIONS_STORAGE_FILE);
        %>

        <div class="myform">
            <% if (questions.size() != 0) {%>
            <div style="margin-left: 20px; padding: 10">Select questions or individual choices to delete.</div>
            <% } else {%>
            <div style="margin-left: 20px; padding: 10">Add the first question!</div>
            <% }%>
            <form name="mainform" action="adminView.jsp" method="POST">
                <ol>

                    <%
                        questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);
                        for (Question question : questions) {
                    %>
                    <div class="question">
                        <li>
                            <input type="checkbox" name="question" value="<%= question%>"/>
                            <%= question%> 
                            <div style="float: right">
                                <%= "Unique Question ID = "%>
                                <input type="button" value="<%= ("" + question.getId())%>" onclick="JavaScript:window.location = 'questionView.jsp?questionId=<%= question.getId()%>';"/>
                                <input type="button" value="Public Link" onclick="copyToClipboard(document.URL.substring(0, document.URL.length - 13) + 'singleQuestion.jsp?questionId=<%= question.getId()%>')">
                            </div> <br>
                            <ol>
                                <% for (Choice choice : question.getChoices()) {%>
                                <li>
                                    <input type="checkbox" name="<%= question%>" value="<%= choice%>"><%= (choice + " (" + choice.getNumberOfUsers() + ((choice.getNumberOfUsers() != 1) ? " votes)" : " vote)"))%></input> <br>
                                </li>
                                <% }%>
                            </ol>
                        </li><br>
                    </div>
                    <% }%>

                </ol>

                <br>
                <div style="margin-left: 20px; padding: 10">
                    New Question : <input type="text" name="newQuestionText" placeholder="Type in new question here"/><br><br>

                    <input type="submit" value="Process" name="submitButton" />
                </div>

            </form>
        </div>

    </body>
</html>
