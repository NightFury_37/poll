<%-- 
    Document   : singleQuestion
    Created on : 16 May, 2013, 12:28:44 PM
    Author     : tpatil
--%>

<%@page import="com.ea.poll.Constants"%>
<%@page import="com.ea.poll.storage.StorageHandler"%>
<%@page import="com.ea.poll.model.Question"%>
<%@page import="java.util.Vector"%>
<%@page import="com.ea.poll.model.User"%>
<%@page import="com.ea.poll.model.Choice"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
        <script type = "text/javascript">

            function xmlhttpPost(strURL, choice) {
                var xmlHttpReq = false;
                var self = this;
// Mozilla/Safari
                if (window.XMLHttpRequest) {
                    self.xmlHttpReq = new XMLHttpRequest();
                }
// IE
                else if (window.ActiveXObject) {
                    self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                self.xmlHttpReq.open('POST', strURL, true);
                self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                self.xmlHttpReq.onreadystatechange = function() {
                    if (self.xmlHttpReq.readyState === 4) {
                        updatepage(self.xmlHttpReq.responseText);
                    }
                };
                self.xmlHttpReq.send(getquerystring());
            }
            
            function getquerystring() {
                var form = document.forms['f1'];
                var word = form.word.value;
                qstr = 'w=' + escape(word); // NOTE: no '?' before querystring
                return qstr;
            }

            function updatepage(str) {
                document.getElementById("result").innerHTML = str;
            }

            window.onload = function() {

                var buttons = fieldset.querySelectorAll('input[type="radio"]');
                var buttonCount = buttons.length;
                var textBox = fieldset.querySelector('input[type="text"]');
                var j;

                for (j = 0; j < buttonCount; j++) {
                    buttons[j].onclick = function() {
                        clearBox(textBox);
                    };
                }

                textBox.onkeypress = function() {
                    for (j = 0; j < buttonCount; j++) {
                        buttons[j].checked = false;
                    }
                };
            };
        </script>
    </head>
    <body>
        <%
            String message = "Question not found.";
            Vector<Choice> choices = new Vector<Choice>();
            Question myQuestion = null;
            Vector<Question> questions = StorageHandler.readQuestions(Constants.QUESTIONS_STORAGE_FILE);
            try {
                if (request.getParameter("questionId") != null) {
                    int id = Integer.parseInt(request.getParameter("questionId"));
                    for (Question question : questions) {
                        if (question.getId() == id) {
                            message = question.getQuestionText();
                            choices = question.getChoices();
                            myQuestion = question;
                        }
                    }
                }
            } catch (NumberFormatException ex) {
            }
        %>

        <div class="controlbar">
            <%
                boolean flag = false;
                if (session.getAttribute("authorized") != null) {
                    if (session.getAttribute("authorized").toString().equals("yes")) {
                        flag = true;
                    }
                }
                if (flag) {%>
            <div class="loginbutton"><a href="administratorLogout.jsp">Administrator Logout</a></div>
            <div class="loginbutton"><a href="adminView.jsp">Back to administration...</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <div class="loginbutton"><b>Hello <%= session.getAttribute("userName")%></b></div>
            <% } else {%>
            <div class="loginbutton"><a href="administratorLogin.jsp">Administrator Login</a></div>
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
            <% }
            %>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Welcome to EA HY Studio Poll</h1>
            </div>
        </div>

        <div class="myform">
            <form name="mainform" action="pollResult.jsp" method="POST">
                <div style="margin-left: 20px; padding: 10px">
                    Name <sup style="color: #FFFF77">*</sup> :
                    <input type="text" name="voterName" placeholder="Enter your name" required="true"></input>
                </div>

                <div style="padding-left: 20px">
                    <h1><%= message%></h1>
                </div>

                <div style="padding-left: 40px">
                    <% for (Choice choice : choices) {%>
                    <div id = "<%= choice.toString()%>">
                        <div style="border: 1px #AAAAAA outset; margin-bottom: 10px; margin-right: 40px; padding: 0px; padding-left: 15px">
                            <h3>
                                <input type = "radio" name="<%= choice.getQuestion()%>" value="<%= choice.toString()%>"><%= choice + " \t(" + choice.getNumberOfUsers() + ((choice.getNumberOfUsers() != 1) ? " votes)" : " vote)")%> <br>
                            </h3>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                    <% }
                        if (myQuestion != null) {%>
                    <div style="border: 1px #AAAAAA outset; margin-bottom: 10px; margin-right: 40px; padding: 10px; padding-left: 10px">
                        <input type="text" name="<%= myQuestion%>Text" placeholder="Enter your choice here&nbsp;">
                    </div>
                    <% }%>
                </div>
                <div style="margin: 40px; padding: 10px; margin-bottom: 20px">
                    <input type="submit" value="Submit your vote!" name="submitButton" style = "margin-left: 40px"/>
                </div>
            </form>
        </div>  
    </body>
</html>
