<%-- 
    Document   : administratorLogin
    Created on : 15 May, 2013, 5:20:55 PM
    Author     : tpatil
--%>

<%@page import="com.ea.poll.control.Authorizer"%>
<%@page import="com.ea.poll.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    if (session.getAttribute("authorized") != null) {
        if (session.getAttribute("authorized").toString().equals("yes")) {
            response.sendRedirect("adminView.jsp");
        }
    }
    if (Authorizer.getInstance().isAuthorized(request.getParameter("userName"), request.getParameter("userPassword"))) {
        session.setAttribute("authorized", "yes");
        session.setAttribute("userName", request.getParameter("userName"));
        response.sendRedirect("adminView.jsp");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <link href="DisplayStyle.css" rel="stylesheet" type="text/css">
        <link rel = "shortcut icon" type = "image/x-icon" href = "resources/ea.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            a:link {color:#FFFFFF;}      /* unvisited link */
            a:visited {color:#FFFFFF;}  /* visited link */
            a:hover {color:#FFFFFF;}  /* mouse over link */
            a:active {color:#FFFFFF;}  /* selected link */
        </style>
        <title>Studio Poll</title>
    </head>
    <body>
        <div class="controlbar">
            <div class="loginbutton"><a href="index.jsp">Back to poll...</a></div>
        </div>

        <div class="header">
            <div class="logo">
                <img src='resources/logo.jpg'/>
            </div>
            <div class="title">
                <h1>Welcome to EA HY Web Poll</h1>
            </div>
        </div>

        <form name="loginform" action="administratorLogin.jsp" method="POST">
            <div class="myform">
                <div style="padding-left: 50px">
                    <h1>Sign In...</h1>
                </div>

                <div style="color: #222222; width: 40%; margin: 0 auto; margin-top: 100px; margin-bottom: 100px; padding: 50px; background: #FFFFFF">
                    <div>
                        <div style="float: left">
                            Administrator Login ID : 
                        </div>
                        <div style="float: right">
                            <input type="text" name="userName" placeholder="Please enter login id here"/>
                        </div>
                    </div>
                    <div style="margin-top: 50px; clear: both"></div>
                    <div>
                        <div style="float: left">
                            Password :
                        </div>
                        <div style="float: right">
                            <input type="password" name="userPassword" placeholder="Please enter password here"/>
                        </div>
                    </div>
                    <div style="margin-bottom: 50px; clear: both"></div>
                    <div style="margin: 0 auto; margin-left: 45%">
                        <input type="submit" name="submitButton" value="Submit"/>
                    </div>
                </div>
            </div> 
        </form>
    </body>
</html>
